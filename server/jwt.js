'use strict';

const jwt = require('express-jwt');
const config = require('./config')();

exports.init = () => {
    return jwt({
        secret: config.session.secret,
        credentialsRequired: true,
        getToken: function fromHeaderOrQuerystring(req) {
            if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'JWT') {
                return req.headers.authorization.split(' ')[1];
            } else if (req.query && req.query.token) {
                return req.query.token;
            }
            return null;
        }
    }).unless({path: [
        '/api/v1/login',
        '/api/v1/register',
        '/api/v1/managers/login'
    ]});
};
