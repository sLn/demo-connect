'use strict';

const fs = require('fs'),
    config = require('./config')(),
    mkdirp = require('mkdirp'),
    os = require('os');

let helpers = module.exports;

helpers.base64ToFile = dataString => {
    return new Promise((resolve, reject) => {
        let matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
            response = {};

        if (matches.length !== 3) {
            return reject(new Error('Invalid input string'));
        }

        response.type = matches[1];
        response.data = new Buffer(matches[2], 'base64');

        let buffer = new Buffer(response.data, 'base64'),
            fileName = (new Date()).valueOf() + '.jpg',
            filePath = config.upload.dest + '/' + fileName;

        fs.writeFile(filePath, buffer, err => {
            if (err) {
                return reject(err);
            }

            return resolve({
                name: fileName,
                path: filePath
            });
        });
    })
};

helpers.checkAndMakeUploadsDir = () => {
    return new Promise((resolve, reject) => {
        fs.access(config.upload.dest, fs.R_OK | fs.W_OK, err => {
            if (!err) {
                return resolve();
            }

            mkdirp(config.upload.dest, err => {
                if (err) {
                    return reject(err);
                }

                return resolve();
            });
        });
    });
};

helpers.getLocalIP = () => {
    let interfaces = os.networkInterfaces(),
        addresses = [];
    
    for (var k in interfaces) {
        for (var k2 in interfaces[k]) {
            let address = interfaces[k][k2];
            if (address.family === 'IPv4' && !address.internal) {
                addresses.push(address.address);
            }
        }
    }

    return addresses.length ? addresses[0] : null;
};
