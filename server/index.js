'use strict';

const helpers = require('./helpers'),
    unless = require('express-unless');
let config = require('./config')();

let express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override');

module.exports.init = function () {
    return new Promise((resolve, reject) => {
        let server = require('http').Server(app);

        app.use(bodyParser.json({ limit: '10mb' }));
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(methodOverride(function (req) {
            if (req.body && typeof req.body === 'object' && '_method' in req.body) {
                // look in urlencoded POST bodies and delete it
                var method = req.body._method;
                delete req.body._method;
                return method;
            }
        }));

        let staticDir = express.static('public');
        app.use(staticDir);

        if (config.logging) {
            app.use((req, res, next) => {
                console.info(req.method, req.path);
                next();
            });
        }

        helpers.checkAndMakeUploadsDir();

//JWT
        app.use((require('./jwt')).init());

        let notification = require("./notification");
        notification.init();

        let models = require('./models');
        models.init((err) => {
            if (err) {
                return reject(err);
            }

            let mosca = require('./mosca')();

            let io = require('./socket')(config.socketPort);
            io.attach(server);

            app.use((req, res, next) => {
                req.io = io;
                req.mosca = mosca;
                next();
            });

            app.locals.title = config.title;
            app.locals.urls = [];

            require('./routes')(app);
        
            staticDir.unless = unless;

            app.use(staticDir.unless(function (req) {
                var ext = url.parse(req.originalUrl).pathname.substr(-4);
                return !~['.jpg', '.html', '.css', '.js'].indexOf(ext);
            }));

            app.use((err, req, res, next) => {
                if (err.name === 'UnauthorizedError') {
                    res.status(401).send({ message: 'invalid token' });
                } else if (err.invalidAttributes) {
                    res.status(err.status || 500).send({
                        message: 'Неправильные данные',
                        details: err.invalidAttributes
                    });
                } else {
                    res.status(500).send({ message: err.message });
                }
            });

            return resolve(server);
        });
    })
};
