/**
 * Created by sanalkhokhlov on 06/05/16.
 */
module.exports = {
    logging: true,
    debug: true,
    debugBEM: true,
    db: {
        name: 'demo-chat_development',
        url: 'localhost/demo-chat_development'
    },

    defaultConnection: 'memory',

    connections: {
        sqlite: {
            adapter: 'waterline-sqlite3',
            filename: root + '/db-dev.sqlite',
            debug: false
        },
        memory: {
            adapter: 'sails-memory'
        }
    }
};