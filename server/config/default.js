var path    = require('path'),
    envPfx  = '', // Префикс для переменных окружения (для многопроектности на одном хосте)
    root    = path.join(__dirname, '../..');

var port = process.env[[envPfx.toUpperCase(), 'NODE', 'PORT'].join('_')] || process.env.NODE_PORT || 3000,
    socketPort = process.env[[envPfx.toUpperCase(), 'SOCKET', 'PORT'].join('_')] || process.env.SOCKET_PORT || 9090,
    hostname = 'localhost:' + port;

module.exports = {
    //App Title
    title: 'demo chat',
    //App port
    port: port,
    //Socket port
    socketPort: socketPort,
    //App hostname
    hostname: hostname,
    //Amount of app workers (processes)
    workers: process.env[[envPfx.toUpperCase(), 'NODE_WORKERS'].join('_')] || require('os').cpus().length,

    // название дефолтного кейсета переводов проекта
    keyset: 'stub',

    debug: false,

    // каталоги со статикой относительно fs
    paths: {
        static : path.join(root, 'static'),
        desktop: path.join(root, 'static', 'desktop.bundles')
    },

    static: {
        // хост для статики относительно клиента
        host: '/'
    },

    // конфиг БЭМ-рендера
    view: {
        templateRoot: path.join(root, 'static'),
        // параметры по умолчанию
        bundleName: 'desktop',
        availableBundles: ['desktop'],
        languageId: 'ru'
    },

    session: {
        secret: 'keyboard cat'
    },
    
    gcm: "AIzaSyDthglZbrx2x1eId5ibzaZm8mVDQDKJvrY",

    upload: {
        dest: path.join(root, 'public', 'upload')
    }
};
