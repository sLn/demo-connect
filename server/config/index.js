/**
 * Created by vaseker on 24.03.14.
 */
var path = require('path'),
    defaultEnv = 'development',
    extend = require('node.extend'),
    _default = require(path.join(__dirname, 'default')),
    config = function config(app) {
        var env = app?
                app.get('env'):
                process.env.NODE_ENV || defaultEnv,
            _config = require(path.join(__dirname, (env)));
        _config.env = env;
        return extend(true, _default, _config);
    };

module.exports = config;
