'use strict';

const Waterline = require('waterline');
let config = require('../config')();

let Channel = Waterline.Collection.extend({
    identity: 'channel',
    connection: 'default',
    schema: true,

    attributes: {
        name: {
            type: 'string',
            required: true
        },

        company_id: {
            model: 'company'
        }
    },

    autoCreatedAt: true,
    autoUpdatedAt: true,
    migrate: 'alter',

    list: function (data) {
        data = data || {};
        return this.find(data);
    },

    getChannelsByCompanyId: function (companyId) {
        return this.find({ company_id: companyId });
    }
});

module.exports = Channel;
