'use strict';

const Waterline = require('waterline');
let config = require('../config')();

let Device = Waterline.Collection.extend({
    identity: 'device',
    connection: 'default',
    schema: true,

    attributes: {
        user_id: {
            model: 'user',
            required: true
        },

        type: {
            type: 'integer',
            required: true
        },

        token: {
            type: 'string',
            required: true
        },

        device_id: {
            type: 'string',
            required: true
        },

        is_active: {
            type: 'boolean',
            defaultsTo: true
        }
    },

    autoCreatedAt: true,
    autoUpdatedAt: true,
    migrate: 'alter',

    list: function (data) {
        data = data || {};
        return this.find(data);
    },

    createOrUpdateDevice: function(userId, data) {
        return new Promise((resolve, reject) => {
            this
                .findOne({
                    user_id: userId,
                    type: data.type,
                    device_id: data.device_id
                })
                .then(
                    device => {
                        if (device) {
                            device.token = data.token;
                            device.is_active = true;
                            device.save(resolve, reject);
                        } else {
                            let newData = data;
                            newData.user_id = userId;
                            this
                                .create(newData)
                                .then(resolve, reject);
                        }
                    }, reject)
        })
    },
    
    disable: function (deviceId) {
        return new Promise((resolve, reject) => {
            this
                .findOne({ device_id: deviceId })
                .then(
                    device => {
                        if (!device) {
                            return resolve();
                        } else {
                            device.is_active = false;
                            device.save(resolve, reject);
                        }
                    }, reject);
        })
    }
});

module.exports = Device;
