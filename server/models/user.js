'use strict';

const Waterline = require('waterline');
const jwt = require('jsonwebtoken');
const config = require('../config')();
const pbkdf2 = require('pbkdf2-sha256');

let User = Waterline.Collection.extend({
    identity: 'user',
    connection: 'default',
    schema: true,

    attributes: {
        name: {
            type: 'string',
            required: true
        },

        phone: {
            type: 'string',
            required: true
        },

        password: {
            type: 'string',
            required: true
        },

        companies: {
            collection: 'company',
            via: 'subscribers',
            dominant: true
        },

        sessions: {
            collection: 'session',
            via: 'user_id'
        },

        toJSON: function() {
            var obj = this.toObject();
            delete obj.password;
            return obj;
        },

        validatePassword: function (password) {
            var parts = this.password.split('$');
            var iterations = parts[1];
            var salt = parts[2];
            return pbkdf2(password + '', new Buffer(salt), iterations, 32).toString('base64') === parts[3];
        },
        
        subscribeCompany: function(companyId) {
            this.companies.add(companyId);
            return this.save();
        },

        unsubscribeCompany: function (companyId) {
            this.companies.remove(companyId);
            return this.save();
        }
    },

    autoCreatedAt: true,
    autoUpdatedAt: true,
    migrate: 'alter',

    beforeCreate: function(values, cb) {
        values.password = this.generatePassword(values.password);
        cb();
    },

    list: function (data) {
        data = data || {};
        return this.find(data);
    },
    
    getById: function (id) {
        return new Promise((resolve, reject) => {
            this
                .findOneById(id)
                .then(
                    user => {
                        if (!user) {
                            return reject(new Error('Пользователь не найден'));
                        }
                        
                        return resolve(user);
                    }, reject)
        });
    },

    /**
     * аутентификация по логину и паролю
     * @param phone
     * @param password
     * @returns {Promise}
     */
    authenticate: function (phone, password) {
        return new Promise((resolve, reject) => {
            this.findOneByPhone(phone).then(
                user => {
                    if (!user) {
                        return reject(new Error('Неправильный номер телефона'));
                    }

                    if (user.validatePassword(password)) {
                        return resolve(user)
                    } else {
                        return reject(new Error('Неправильный пароль'));
                    }
                }, reject)
        })
    },

    authenticateUser: function (phone, password) {
        return new Promise((resolve, reject) => {
            this.authenticate(phone, password).then(
                user => {
                    let data = {
                        userId: user.id
                    };
                    return resolve(jwt.sign(data, config.session.secret));
                }, reject)
        })
    },

    authenticateManager: function (phone, password) {
        return new Promise((resolve, reject) => {
            this.authenticate(phone, password).then(
                user => {
                    let Manager = require('./index').Manager;

                    Manager.findOne({ user_id: user.id }).then(
                        manager => {
                            if (!manager) {
                                return reject(new Error('Пользователь не является менеджером'));
                            }

                            let data = {
                                userId: user.id,
                                managerId: manager.id,
                                companyId: manager.company_id,
                                isAdmin: manager.is_admin
                            };
                            return resolve(jwt.sign(data, config.session.secret));
                        }, reject);
                }, reject)
        })
    },

    makeSalt: function () {
        return Math.round((new Date().valueOf() * Math.random())) + ''
    },

    generatePassword: function (password) {
        var salt = this.makeSalt(),
            iterations = 10000,
            hashedPassword = pbkdf2(password, new Buffer(salt), iterations, 32).toString('base64');
        return ['pbkdf2_sha256', iterations, salt, hashedPassword].join('$');
    },

    register: function (data) {
        return new Promise((resolve, reject) => {
            this.findOneByPhone(data.phone).then(
                user => {
                    if (user) {
                        return reject(new Error('Пользователь с таким телефоном уже зарегистрирован'))
                    }

                    this.create(data).then(resolve, reject);

                    return null;
                }, reject);
        })
    },

    getSubscribedCompanies: function (id) {
        return new Promise((resolve, reject) => {
            this
                .findOneById(id)
                .populate('companies')
                .then(
                    user => resolve(user.companies),
                    reject)
        })
    }

});

module.exports = User;
