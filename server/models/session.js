'use strict';

const Waterline = require('waterline');
let config = require('../config')();

let Session = Waterline.Collection.extend({
    identity: 'session',
    connection: 'default',
    schema: true,

    attributes: {
        user_id: {
            model: 'user',
            required: true
        },
        
        app_version: {
            type: 'string'
        },

        hardware: {
            type: 'string'
        },

        os_type: {
            type: 'string'
        },

        os_version: {
            type: 'string'
        },

        ip: {
            type: 'string'
        },
        
        is_online: {
            type: 'boolean',
            defaultsTo: false
        },

        session_id: {
            type: 'string'
        }
    },

    autoCreatedAt: true,
    autoUpdatedAt: true,
    migrate: 'alter',

    list: function (data) {
        data = data || {};
        return this.find(data);
    },

    getListByUserId: function (userId) {
        return this.find({ user_id: userId });
    },

    enable: function (data) {
        return new Promise((resolve, reject) => {
            this
                .findOne({
                    user_id: data.user_id,
                    session_id: data.session_id
                })
                .then(
                    session => {
                        if (session) {
                            session.is_online = true;
                            session.save(resolve, reject);
                        } else {
                            this
                                .create(data)
                                .then(resolve, reject);
                        }
                    }, reject)
        })
    },

    disable: function (sessionId) {
        return new Promise((resolve, reject) => {
            this
                .findOne({ session_id: sessionId })
                .then(
                    session => {
                        if (!session) {
                            return resolve();
                        }
                        
                        session.is_online = false;
                        session.save(resolve, reject);
                    }, reject);
        });
    }
});

module.exports = Session;
