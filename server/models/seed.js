'use strict';

const async = require('async');
let models = require('./index');
let Category = models.Category;
let Company = models.Company;
let User = models.User;
let Manager = models.Manager;

let categories = [
    {
        id: 1,
        name: 'Салоны красоты',
        companies: [
            {
                id: 1,
                name: 'Винтаж',
                about: '',
                channels: [
                    {
                        id: 1,
                        name: 'Общие вопросы'
                    },
                    {
                        id: 2,
                        name: 'Новости'
                    }
                ]
            }
        ]
    },
    {
        id: 2,
        name: 'Рестораны',
        companies: [
            {
                id: 1,
                name: 'Александр',
                about: '',
                channels: [
                    {
                        id: 3,
                        name: 'Общие вопросы'
                    },
                    {
                        id: 4,
                        name: 'Новости'
                    },
                    {
                        id: 5,
                        name: 'Заказать столик'
                    },
                    {
                        id: 6,
                        name: 'Доставка еды'
                    }
                ]
            }
        ]
    },
    {
        id: 3,
        name: 'Магазины одежды',
        companies: [
            {
                id: 1,
                name: 'Reserved',
                about: '',
                channels: [
                    {
                        id: 7,
                        name: 'Общие вопросы'
                    },
                    {
                        id: 8,
                        name: 'Новости'
                    }
                ]
            }
        ]
    },
    {
        id: 4,
        name: 'Гостиницы',
        companies: [
            {
                id: 1,
                name: 'DoubleTree',
                about: '',
                channels: [
                    {
                        id: 9,
                        name: 'Общие вопросы'
                    },
                    {
                        id: 10,
                        name: 'Новости'
                    }
                ]
            }
        ]
    },
    {
        id: 5,
        name: 'Строительные магазины',
        companies: [
            {
                id: 1,
                name: 'Строительный двор',
                about: '',
                channels: [
                    {
                        id: 11,
                        name: 'Общие вопросы'
                    },
                    {
                        id: 12,
                        name: 'Новости'
                    },
                    {
                        id: 13,
                        name: 'Доставка товара'
                    }
                ]
            }
        ]
    },
    {
        id: 6,
        name: 'СТО/Автомойки',
        companies: [
            {
                id: 1,
                name: 'Кит',
                about: '',
                channels: [
                    {
                        id: 14,
                        name: 'Общие вопросы'
                    },
                    {
                        id: 15,
                        name: 'Новости'
                    }
                ]
            }
        ]
    }
];

let users = [
    {
        name: 'Санал Хохлов',
        phone: '79995402480',
        password: 123
    },
    {
        name: 'Иван Иванов',
        phone: '79998887766',
        password: 123
    }
];

module.exports = () => {
    return new Promise((resolve, reject) => {
        async.parallel([
            next => {
                async.map(categories, (c, n) => {
                    Category.create({ name: c.name }).then(newCategory => {
                        async.map(c.companies, (company, next) => {
                            Company.create({ name: company.name, category_id: newCategory.id }).then(newCompany => {
                                // next(null, newCompany);

                                company.channels.forEach(ch => newCompany.channels.add({ name: ch.name }));
                                newCompany.save().then(() => next(null), next);

                                return null;

                            }, next);
                        }, n);

                        return null;
                    }, n);
                }, (err, result) => {
                    if (err) {
                        return next(err);
                    }
                    next(null, result);
                });
            },

            next => {
                async.map(users, (u, n) => {
                    User.create(u).then(user => n(null, user), n);
                }, (err, result) => {
                    if (err) {
                        return next(err);
                    }
                    next(null, result);
                });
            },

            next => {
                Manager.create({
                    company_id: 1,
                    user_id: 2
                }).then(manager => next(null, manager), next);
            }
        ], (err, result) => {
            if (err) {
                return reject(err);
            }

            console.log('Тестовые данные загружены');
            resolve();
        })
    })
};