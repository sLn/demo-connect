'use strict';

const Waterline = require('waterline');
let config = require('../config')();

let Company = Waterline.Collection.extend({
    identity: 'company',
    connection: 'default',
    schema: true,

    attributes: {
        name: {
            type: 'string',
            required: true
        },

        category_id: {
            model: 'category'
        },
        
        channels: {
            collection: 'channel',
            via: 'company_id'
        },

        managers: {
            collection: 'manager',
            via: 'company_id'
        },

        subscribers: {
            collection: 'user',
            via: 'companies',
            dominant: true
        },

        createChannel: function (data) {
            this.channels.add({ name: data.name });
            return this.save();
        }
    },

    autoCreatedAt: true,
    autoUpdatedAt: true,
    migrate: 'alter',

    list: function (data) {
        data = data || {};
        return this.find(data);
    },

    getById: function (id, userId) {
        return new Promise((resolve, reject) => {
             this
                .findOneById(id)
                .populate('channels')
                .populate('subscribers', { id: userId })
                 .then(
                     company => {
                         company.is_subscribed = company.subscribers.length > 0;
                         delete company.subscribers;

                         return resolve(company);
                     }, reject);
        })
    }
});

module.exports = Company;
