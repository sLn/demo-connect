'use strict';

let config = require('../config')();
const Waterline = require('waterline');
const orm = new Waterline();

function init(next) {
    let adapters = {};
    Object.keys(config.connections).forEach(key => {
        let connection = config.connections[key];
        adapters[key] = require(connection.adapter);
    });
    
    var conf = {
        adapters: adapters,
        connections: {
            default: {
                adapter: config.defaultConnection
            }
        }
    };

    orm.loadCollection(require('./category'));
    orm.loadCollection(require('./company'));
    orm.loadCollection(require('./channel'));
    orm.loadCollection(require('./chat'));
    orm.loadCollection(require('./message'));
    orm.loadCollection(require('./user'));
    orm.loadCollection(require('./manager'));
    orm.loadCollection(require('./device'));
    orm.loadCollection(require('./session'));
    if (!orm.connections) {
        orm.initialize(conf, function (err, models) {
            if (err) {
                return next(err);
            }
            setExports();

            let seed = require('./seed');
            seed().then(() => next(null), next);
        });
    } else {
        setExports();
        next();
    }
}

function setExports() {
    exports.Category = orm.collections.category;
    exports.Company = orm.collections.company;
    exports.Channel = orm.collections.channel;
    exports.Chat = orm.collections.chat;
    exports.Message = orm.collections.message;
    exports.User = orm.collections.user;
    exports.Manager = orm.collections.manager;
    exports.Device = orm.collections.device;
    exports.Session = orm.collections.session;
    // exports.User = orm.collections.user;
    // exports.oauth = require('./oauth');
}

exports.init = init;
