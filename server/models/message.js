'use strict';

const Waterline = require('waterline'),
    helpers = require('../helpers'),
    config = require('../config')();

let Message = Waterline.Collection.extend({
    identity: 'message',
    connection: 'default',
    schema: true,

    attributes: {
        user_id: {
            model: 'user',
            required: true
        },

        text: {
            type: 'string',
            defaultsTo: ''
        },

        chat_id: {
            model: 'chat'
        },

        channel_id: {
            model: 'channel'
        },

        image: {
            type: 'string',
            defaultsTo: null
        },

        unread: {
            type: 'boolean',
            defaultsTo: true
        }
    },

    autoCreatedAt: true,
    autoUpdatedAt: true,
    migrate: 'alter',

    afterCreate: function (values, next) {
        let Chat = require('../models').Chat;

        Chat.findOneById(values.chat_id).then(
            chat => {
                chat.status = 'open';
                chat.save().then(
                    () => next(),
                    next);
            }, next);
    },

    list: function (data) {
        data = data || {};
        return this.find(data);
    },

    beforeCreate: function(values, cb) {
        if (values['image-src']) {
            helpers.base64ToFile(values['image-src']).then(
                result => {
                    let host = helpers.getLocalIP(),
                        port = config.port;
                    values.image = `http://${host}:${port}/upload/${result.name}`;
                    cb();
                },
                err => {
                    cb(err);
                })
        } else {
            cb();
        }
    },

    getDeliveriesByChannelId: function(channelId) {
        return new Promise((resolve, reject) => {
            this
                .find({
                    channel_id: channelId,
                    chat_id: null
                })
                .populate('user_id')
                .then(
                    deliveries => {
                        resolve(deliveries.map(d => {
                            d.user = d.user_id;
                            delete d.user_id;
                            return d;
                        }))
                    }, reject);
        })
    },

    getMessages: function (data) {
        return new Promise((resolve, reject) => {
            this
                .list({
                    where: {
                        or: [
                            { chat_id: data.chatId },
                            { channel_id: data.channelId }
                        ]
                    },
                    sort: 'createdAt DESC'
                })
                .populate('user_id')
                .then(
                    messages => {
                        return resolve(messages.map(m => {
                            m.user = m.user_id;
                            delete m.user_id;
                            return m;
                        }))
                    }, reject);
        });
    },

    createMessage: function (data) {
        return new Promise((resolve, reject) => {
            this
                .create(data)
                .then(
                    message => {
                        let User = require('./index').User;

                        User
                            .findOneById(message.user_id)
                            .then(
                                user => {
                                    message.user = user;
                                    return resolve(message);
                                }, reject);
                    }, reject);
        })
    },

    readMessage: function (messageId) {
        return this.update({ id: messageId }, { unread: false });
    }
});

module.exports = Message;