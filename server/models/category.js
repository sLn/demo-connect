'use strict';

const Waterline = require('waterline');
let config = require('../config')();

let Category = Waterline.Collection.extend({
    identity: 'category',
    connection: 'default',
    schema: true,

    attributes: {
        name: {
            type: 'string',
            required: true
        }
    },

    autoCreatedAt: true,
    autoUpdatedAt: true,
    migrate: 'alter',

    list: function (data) {
        data = data || {};
        return this.find(data);
    }
});

module.exports = Category;
