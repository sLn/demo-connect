'use strict';

const Waterline = require('waterline');

let Manager = Waterline.Collection.extend({
    identity: 'manager',
    connection: 'default',
    schema: true,

    attributes: {
        company_id: {
            model: 'company'
        },

        user_id: {
            model: 'user',
            unique: true
        },

        is_admin: {
            type: 'boolean',
            defaultsTo: false
        }
    },

    autoCreatedAt: true,
    autoUpdatedAt: true,
    migrate: 'alter',

    list: function (data) {
        data = data || {};
        return this.list(data);
    },

    getManagersByCompanyId: function (companyId) {
        return new Promise((resolve, reject) => {
            this
                .find({ company_id: companyId })
                .populate('user_id')
                .then(
                    managers => {
                        resolve(managers.map(m => {
                            m.user = m.user_id;
                            delete m.user_id;
                            return m;
                        }))
                    }, reject);
        })
    },

    register: function (data) {
        return new Promise((resolve, reject) => {
            let User = require('./index').User;

            User.register({
                name: data.name,
                phone: data.phone,
                password: data.password
            }).then(
                user => {
                    this.create({
                        company_id: data.company_id,
                        is_admin: data.is_admin,
                        user_id: user.id
                    }).then(
                        manager => {
                            manager.user = user;
                            delete manager.user_id;

                            return resolve(manager);
                        }, reject);
                }, reject);
        });
    }
});

module.exports = Manager;
