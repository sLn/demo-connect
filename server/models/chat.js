'use strict';

const Waterline = require('waterline');
const async = require('async');
let Notification = require('../notification');

let statuses = ['open', 'in-work', 'closed'];

let Chat = Waterline.Collection.extend({
    identity: 'chat',
    connection: 'default',
    schema: true,

    attributes: {
        user_id: {
            model: 'user',
            required: true
        },
        
        channel_id: {
            model: 'channel'
        },
        
        manager_id: {
            model: 'manager',
            defaultsTo: null
        },

        messages: {
            collection: 'message',
            via: 'chat_id'
        },

        status: {
            type: 'string',
            enum: statuses,
            defaultsTo: 'open',
            required: true
        },

        setManagerId: function (managerId) {
            this.manager_id = managerId;
            this.status = 'in-work';
            return this.save();
        },

        getUserIdForNotification: function (fromUser) {
            return new Promise((resolve, reject) => {
                if (fromUser !== this.user_id) {
                    return resolve(this.user_id);
                } else {
                    let Manager = require('./index').Manager;

                    Manager.findOneById(this.manager_id).then(
                        manager => {
                            if (!manager) {
                                return reject(new Error('Такого менеджера не существует'));
                            }

                            resolve(manager.user_id);
                        }, reject);
                }
            });
        },

        sendNotification: function(data) {
            return new Promise((resolve, reject) => {
                this.getUserIdForNotification(data.fromUser).then(
                    toUser => {
                        let Device = require('./index').Device;
                        Device.find({
                            user_id: toUser,
                            is_active: true
                        }).then(
                            devices => {
                                if (!devices.length) {
                                    return resolve();
                                }

                                Notification.send(devices.map(d => d.token), {
                                    message: data.message.text,
                                    view: 'chat',
                                    item: this.id
                                }).then(resolve, reject);
                            }, reject);
                    }, reject);
            });
        },

        /**
         * can close chat
         * @param data = { userId: 1, managerId: 1 }
         * @returns {Promise}
         */
        canClose: function (data) {
            return (data.userId == this.user_id || (data.managerId && data.managerId == this.manager_id));
        },

        close: function () {
            this.status = 'closed';
            this.manager_id = null;
            return this.save();
        }
    },

    autoCreatedAt: true,
    autoUpdatedAt: true,
    migrate: 'alter',

    list: function (data) {
        data = data || {};
        return this.find(data);
    },

    getById: function (id) {
        return this.findOneById(id);
    },

    getOrCreate: function (channelId, userId) {
        return new Promise((resolve, reject) => {
            this.findOrCreate({
                channel_id: channelId,
                user_id: userId
            })
                .then(
                chat => {
                    this
                        .findOneById(chat.id)
                        .populate('channel_id').then(
                            chat => {
                                chat.channel = chat.channel_id;
                                chat.channel_id = chat.channel.id;

                                let Company = require('./index').Company;
                                Company
                                    .findOneById(chat.channel.company_id)
                                    .then(
                                        company => {
                                            chat.company = company;
                                            return resolve(chat);
                                        }, reject);
                            }, reject);
                    }, reject
            );
        })
    },

    getChatsByUserId: function (userId) {
        return new Promise((resolve, reject) => {
            this
                .find({ where: { user_id: userId }, sort: 'updatedAt DESC' })
                .populate('channel_id')
                .populate('messages', { limit: 1, sort: 'createdAt DESC' })
                .then(
                    chats => {
                        let Company = require('./index').Company;

                        async.map(chats, (c, n) => {
                            c.channel = c.channel_id;
                            delete c.channel_id;
                            Company.findOneById(c.channel.company_id).then(
                                company => {
                                    c.company = company;
                                    n(null);
                                }, n)
                        }, (err, result) => {
                            if (err) {
                                return reject(err);
                            }

                            resolve(this.sortByLastMessage(chats));
                        });
                    }, reject);
        })
    },

    getChatsByManagerId: function (managerId) {
        return new Promise((resolve, reject) => {
            this
                .find({ where: { manager_id: managerId }, sort: 'updatedAt DESC' })
                .populate(['channel_id', 'user_id'])
                .populate('messages', { limit: 1, sort: 'createdAt DESC' })
                .then(
                    chats => {
                        let chatsMap = chats.map(c => {
                            c.channel = c.channel_id;
                            delete c.channel_id;

                            c.user = c.user_id;
                            delete c.user_id;

                            return c;
                        });

                        resolve(this.sortByLastMessage(chatsMap));
                    }, reject);
        })
    },

    getOpenChatsByCompanyId: function (companyId) {
        return new Promise((resolve, reject) => {
            let Channel = require('../models').Channel;

            Channel
                .find({ company_id: companyId })
                .then(
                    channels => {
                        let channelsId = channels.map(ch => ch.id);

                        this
                            .find({
                                where: {
                                    channel_id: channelsId,
                                    manager_id: null,
                                    status: 'open'
                                },
                                sort: 'updatedAt DESC'
                            })
                            .populate(['channel_id', 'user_id'])
                            .populate('messages', { limit: 1, sort: 'createdAt DESC' })
                            .then(
                                chats => {
                                    let chatsMap = chats.map(c => {
                                        c.channel = c.channel_id;
                                        delete c.channel_id;

                                        c.user = c.user_id;
                                        delete c.user_id;

                                        return c;
                                    });

                                    resolve(this.sortByLastMessage(chatsMap));
                                }, reject);
                    }, reject);
        })
    },

    getUserIdsForNotification: function (chatId) {
        return new Promise((resolve, reject) => {
            console.dir(chatId)
            this.findOneById(chatId).then(
                chat => {
                    if (!chat) {
                        return reject(new Error('Чат не найден'));
                    }
                    
                    if (!chat.manager_id) {
                        return resolve([chat.user_id]);
                    }
                    
                    let Manager = require('./index').Manager;

                    Manager.findOneById(chat.manager_id).then(
                        manager => {
                            if (!manager) {
                                return reject(new Error('Такого менеджера не существует'));
                            }

                            return resolve([manager.user_id, chat.user_id]);
                        }, reject);
                }, reject);
        });
    },

    //TODO: переделать когда база будет SQL
    sortByLastMessage: function (chats) {
        chats.sort((l, r) => {
            if (!l.messages.length) {
                return 1;
            }

            if (!r.messages.length) {
                return -1;
            }

            return l.messages[0].createdAt < r.messages[0].createdAt;
        });

        return chats;
    },

    //TODO: страх и ненависть. ПЕРЕДЕЛАТЬ!!!
    sendNotificationToAllManagers: function (chatId) {
        return new Promise((resolve, reject) => {
            this.findOne({ id: chatId, manager_id: null }).then(
                chat => {
                    if (!chat) {
                        return resolve();
                    } else {
                        //get company
                        let Channel = require('./index').Channel;

                        Channel.findOneById(chat.channel_id).then(
                            channel => {
                                let Manager = require('./index').Manager;

                                Manager.find({ company_id: channel.company_id }).then(
                                    managers => {
                                        if (!managers.length) {
                                            return resolve();
                                        } else {
                                            let Device = require('./index').Device;

                                            Device.find({ user_id: managers.map(m => m.user_id), is_active: true }).then(
                                                devices => {
                                                    if (!devices.length) {
                                                        return resolve();
                                                    }

                                                    Notification.send(devices.map(d => d.token), {
                                                        message: `Новое сообщение в чате "${channel.name}"`,
                                                        view: 'chat',
                                                        item: chatId
                                                    }).then(resolve, reject);
                                                }, reject);
                                        }
                                    }, reject);
                            }, reject);
                    }
                }, reject);
        })
    },

    getStatuses: function () {
        return statuses;
    }

});

module.exports = Chat;
