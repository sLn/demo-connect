'use strict';

let ctrl = module.exports,
    models = require('../models'),
    User = models.User,
    Device = models.Device;

ctrl.login = (req, res, next) => {
    User.authenticateUser(req.body.phone, req.body.password).then(
        token => res.send({ token: token }), next);
};

ctrl.register = (req, res, next) => {
    User.register(req.body).then(user => res.send(user), next);
};

ctrl.loginManager = (req, res, next) => {
    User.authenticateManager(req.body.phone, req.body.password).then(
        token => res.send({ token: token }), next);
};

ctrl.createDevice = (req, res, next) => {
    Device.createOrUpdateDevice(req.user.userId, req.body).then(() => res.send({ status: 'ok' }), next);
};

ctrl.getAllDevices = (req, res, next) => {
    Device.find().then(devices => res.send(devices), next);
};

ctrl.disableDevice = (req, res, next) => {
    Device.disable(req.params.deviceId).then(() => res.send({ status: 'ok' }), next);
};
