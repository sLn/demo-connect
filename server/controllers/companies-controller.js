'use strict';

let ctrl = module.exports,
    models = require('../models'),
    Company = models.Company,
    Channel = models.Channel;

ctrl.show = (req, res, next) => {
    Company.getById(req.params.id, req.user.userId).then(
        company => {
            if (!company) {
                return res.status(404).send('Not found');
            }

            res.send(company);
        }, next);
};

ctrl.createChannel = (req, res, next) => {
    let newChannelData = req.body;
    newChannelData.company_id = req.params.id;
    Channel.create(newChannelData).then(channel => res.send(channel), next);
};

ctrl.getChannelsByCompanyId = (req, res, next) => {
    Channel.getChannelsByCompanyId(req.params.id).then(ch => res.send(ch), next);
};
