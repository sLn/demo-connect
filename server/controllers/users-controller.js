'use strict';

let ctrl = module.exports,
    models = require('../models'),
    User = models.User,
    Session = models.Session;

ctrl.getSubscribedCompanies = (req, res, next) => {
    User.getSubscribedCompanies(req.params.userId).then(
        companies => res.send(companies),
        next);
};

ctrl.subscribeCompany = (req, res, next) => {
    User.getById(req.params.userId).then(
        user => user.subscribeCompany(req.params.companyId).then(
            () => res.send({ status: 'ok' }),
            next),
        next);
};

ctrl.unsubscribeCompany = (req, res, next) => {
    User.getById(req.params.userId).then(
        user => user.unsubscribeCompany(req.params.companyId).then(
            () => res.send({ status: 'ok' }),
            next),
        next);
};

ctrl.getSessions = (req, res, next) => {
    Session.getListByUserId(req.params.id).then(s => res.send(s), next);
};
