'use strict';

let ctrl = module.exports = {},
    models = require('../models'),
    Category = models.Category,
    Company = models.Company;

ctrl.index = (req, res, next) => {
    Category.list().then(
        categories => {
            res.send(categories);
        }, next);
};

ctrl.show = (req, res, next) => {
    let criteria = {
        where: {
            category_id: req.params.id
        }
    };

    Company.list(criteria).then(
        companies => {
            res.send(companies);
        }, next);
};
