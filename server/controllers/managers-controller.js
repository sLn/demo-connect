'use strict';

let ctrl = module.exports,
    models = require('../models'),
    Manager = models.Manager;

ctrl.getManagersByCompanyId = (req, res, next) => {
    Manager.getManagersByCompanyId(req.params.id).then(managers => res.send(managers), next);
};

ctrl.create = (req, res, next) => {
    let data = {
        name: req.body.name,
        phone: req.body.phone,
        password: req.body.password,
        company_id: req.params.id,
        is_admin: req.body.is_admin
    };
    Manager.register(data).then(user => res.send(user), next);
};
