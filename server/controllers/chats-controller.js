'use strict';

let ctrl = module.exports = {},
    models = require('../models'),
    Chat = models.Chat,
    Message = models.Message;

ctrl.index = (req, res, next) => {
    Chat.list().then(
        chats => {
            res.send(chats);
        }, next);
};

ctrl.getChat = (req, res, next) => {
    Chat.getById(req.params.id).then(
        chat => {
            if (!chat) {
                return res.status(404).send('Not found');
            }

            req.chat = chat;
            next();
        }, next);
};

/*ctrl.show = (req, res) => {
    res.send(req.chat.messages);
};*/

ctrl.getMessages = (req, res, next) => {
    Message
        .getMessages({
            chatId: req.chat.id,
            channelId: req.chat.channel_id
        })
        .then(messages => res.send(messages), next);
};

ctrl.createMessage = (req, res, next) => {
    let chatId = req.params.id,
        messageData = {
            chat_id: chatId,
            user_id: req.user.userId,
            text: req.body.text,
            image: req.body.image,
            'image-src': req.body['image-src']
        };

    Message.createMessage(messageData).then(
        message => {
            req.io.to(chatId).emit('message:new', message);

            //send message through mosca
            if (req.mosca) {
                Chat
                    .getUserIdsForNotification(chatId)
                    .then(
                        userIds => {
                            userIds.forEach(u => {
                                req.mosca.publish({
                                    topic: 'users/' + u,
                                    payload: JSON.stringify({
                                        type: 'message:new',
                                        body: message
                                    }),
                                    qos: 0,
                                    retain: false
                                })
                            });
                        },
                        err => {
                            console.dir(err);
                        });
            }

            //push
            req.chat
                .sendNotification({
                    fromUser: req.user.userId,
                    message: message
                }).then(
                result => console.dir(result),
                err => {
                    if (err) {
                        console.dir(err);
                    }
                }
            );

            //send push to all managers
            Chat
                .sendNotificationToAllManagers(chatId)
                .then(
                    result => console.dir(result),
                    err => {
                        if (err) {
                            console.dir(err);
                        }
                    }
                );

            if (req.user.managerId && !req.chat.manager_id) {
                req.chat.setManagerId(req.user.managerId).then(
                    () => res.send(message),
                    next);
            } else {
                return res.send(message);
            }
        }, next);
};

ctrl.getDeliveriesByChannelId = (req, res, next) => {
    Message.getDeliveriesByChannelId(req.params.id).then(ds => res.send(ds), next);
};

ctrl.createDelivery = (req, res, next) => {
    let channelId = req.params.id;

    Message.create({
        channel_id: channelId,
        chat_id: null,
        user_id: req.user.userId,
        text: req.body.text
    }).then(message => res.send(message), next);

    //TODO: отправить уведомление все подписчикам компании
};

ctrl.create = (req, res, next) => {
    Chat.create(req.body).then(chat => res.send(chat), next);
};

ctrl.getChatsByUserId = (req, res, next) => {
    Chat.getChatsByUserId(req.params.id).then(chats => res.send(chats), next);
};

ctrl.getChatsByManagerId = (req, res, next) => {
    Chat.getChatsByManagerId(req.params.id).then(chats => res.send(chats), next);
};

ctrl.getOpenChatsByCompanyId = (req, res, next) => {
    Chat.getOpenChatsByCompanyId(req.params.id).then(chats => res.send(chats), next);
};

ctrl.setChatManagerId = (req, res, next) => {
    req.chat.setManagerId(req.params.managerId).then(() => res.send({ status: 'ok' }), next);
};

ctrl.getOrCreate = (req, res, next) => {
    let channelId = req.params.channelId;
    let userId = req.params.userId;
    
    Chat.getOrCreate(channelId, userId).then(chat => res.send(chat), next);
};

ctrl.closeChat = (req, res, next) => {
    if (req.chat.canClose(req.user)) {
        req.chat.close().then(
            () => res.send({ status: 'ok' }),
            next)
    } else {
        return next(new Error('Нет прав'));
    }
};
