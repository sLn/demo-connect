'use strict';

let ctrl = module.exports,
    models = require('../models'),
    Channel = models.Channel;

ctrl.update = (req, res, next) => {
    Channel.update({ id: req.params.id }, req.body).then(
        channel => res.send(channel),
        next);
};
