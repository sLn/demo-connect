'use strict';

let ctrl = module.exports,
    models = require('../models'),
    Chat = models.Chat,
    Message = models.Message;

ctrl.index = (req, res, next) => {
    Message.find().then(messages => res.send(messages), next);
};

ctrl.readMessage = (req, res, next) => {
    Message
        .readMessage(req.params.id)
        .then(
            messages => {
                if (!messages.length) {
                    return next(new Error('Сообщение не найдено'));
                } else {
                    let message = messages[0];

                    if (req.mosca) {
                        Chat
                            .getUserIdsForNotification(message.chat_id)
                            .then(
                                userIds => {
                                    userIds.forEach(u => {
                                        req.mosca.publish({
                                            topic: 'users/' + u,
                                            payload: JSON.stringify({
                                                type: 'message:read',
                                                body: message
                                            }),
                                            qos: 0,
                                            retain: false
                                        })
                                    });
                                },
                                err => {
                                    console.dir(err);
                                });
                    }

                    res.send(message);
                }
            }, next);
};
