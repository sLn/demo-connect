'use strict';

let Notification = {},
    gcm = require('node-gcm'),
    config = require('./config')(),
    sender = null;

Notification.init = function () {
    sender = new gcm.Sender(config.gcm);
};

/**
 * 
 * @param tokens
 * @param data - {
 *          message
 *          view
 *          item
 *      }
 * @returns {Promise}
 */
Notification.send = function (tokens, data) {
    data = data || {};
    var message = new gcm.Message({
        priority: 'high',
        timeToLive: 3,
        content_available: true,
        notification: {
            sound: 'default',
            body: data.message
        },
        data: data
    });

    return new Promise((resolve, reject) => {
        sender.send(message, tokens, 4, function (err, response) {
            if (err) {
                return reject(err);
            }
            return resolve(response);
        });
    })
};

module.exports = Notification;