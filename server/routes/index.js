'use strict';

var Routes = function (app) {
    var routes = {};
    app.set('routes', routes);

    routes.api = require('./api')(app);

    for (var route in routes) {
        if (routes.hasOwnProperty(route)) {
            var rt = routes[route];
            if (rt && typeof rt.urls === 'object') {
                app.locals.urls = app.locals.urls.concat(rt.urls);
            }
            if (app.get('init')) {
                /*jshint maxdepth: 8*/
                var beforeLoaders = ['controllers', 'models'];
                for (var _scope in rt) {
                    if (beforeLoaders.indexOf(_scope) > -1 && rt.hasOwnProperty(_scope)) {
                        var scope = rt[_scope];
                        for (var modName in scope){
                            if (scope.hasOwnProperty(modName)) {
                                var mod = scope[modName];
                                if (mod.beforeStart) {
                                    mod.beforeStart();
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    app.locals.urls.sort(function (a, b) {
        return a.pos - b.pos;
    });
};
module.exports = Routes;