'use strict';

module.exports = app => {
    let express = require('express'),
        router = express.Router();

    let UsersController = require('../controllers/users-controller'),
        ChatsController = require('../controllers/chats-controller');

    router
        .get('/users/:userId/companies', UsersController.getSubscribedCompanies)
        .post('/users/:userId/subscribe/:companyId', UsersController.subscribeCompany)
        .delete('/users/:userId/unsubscribe/:companyId', UsersController.unsubscribeCompany);

    router
        .get('/users/:id/chats', ChatsController.getChatsByUserId);

    router
        .get('/users/:id/sessions', UsersController.getSessions);

    return router;
};
