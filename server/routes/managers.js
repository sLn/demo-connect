'use strict';

module.exports = app => {
    let express = require('express'),
        router = express.Router();

    let ChatsController = require('../controllers/chats-controller');
    
    router
        .get('/managers/:id/chats', ChatsController.getChatsByManagerId);

    return router;
};
