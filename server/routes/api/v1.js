'use strict';

module.exports = app => {
    let express = require('express'),
        router = express.Router();
    
    router.use((require('../auth'))(app));
    router.use((require('../categories'))(app));
    router.use((require('../channels'))(app));
    router.use((require('../chats'))(app));
    router.use((require('../companies'))(app));
    router.use((require('../managers'))(app));
    router.use((require('../users'))(app));
    router.use((require('../messages'))(app));

    return router;
};
