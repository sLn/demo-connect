'use strict';

module.exports = app => {
    let express = require('express'),
        router = express.Router();

    let ChannelsController = require('../controllers/channels-controller'),
        ChatsController = require('../controllers/chats-controller');

    router
        .put('/channels/:id', ChannelsController.update);

    router
        .get('/channels/:id/deliveries', ChatsController.getDeliveriesByChannelId)
        .post('/channels/:id/deliveries', ChatsController.createDelivery)
        .get('/channels/:channelId/user/:userId', ChatsController.getOrCreate);

    return router;
};
