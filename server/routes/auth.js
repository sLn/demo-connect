'use strict';

module.exports = app => {
    let express = require('express'),
        router = express.Router();
    
    let AuthController = require('../controllers/auth-controller');
    
    router
        .post('/login', AuthController.login)
        .post('/register', AuthController.register);
    
    router
        .post('/managers/login', AuthController.loginManager);
    
    router
        .get('/devices', AuthController.getAllDevices)
        .post('/devices', AuthController.createDevice)
        .delete('/devices/:deviceId', AuthController.disableDevice);

    return router;
};
