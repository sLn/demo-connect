'use strict';

module.exports = app => {
    let express = require('express'),
        router = express.Router();

    let MessagesController = require('../controllers/messages-controller');
    
    router
        .get('/messages', MessagesController.index)
        .post('/messages/:id/read', MessagesController.readMessage);

    return router;
};
