'use strict';

module.exports = app => {
    let express = require('express'),
        router = express.Router();

    let CompaniesController = require('../controllers/companies-controller'),
        ManagersController = require('../controllers/managers-controller'),
        ChatsController = require('../controllers/chats-controller');
    
    router
        .get('/companies/:id', CompaniesController.show);
        
    router
        .get('/companies/:id/channels', CompaniesController.getChannelsByCompanyId)
        .post('/companies/:id/channels', CompaniesController.createChannel);

    router
        .get('/companies/:id/managers', ManagersController.getManagersByCompanyId)
        .post('/companies/:id/managers', ManagersController.create);
    
    router
        .get('/companies/:id/chats/open', ChatsController.getOpenChatsByCompanyId);

    return router;
};
