'use strict';

module.exports = app => {
    let express = require('express'),
        router = express.Router();

    let ChatsController = require('../controllers/chats-controller');
    
    router
        .get('/chats', ChatsController.index);
    
    router
        .post('/chats/:id/managers/:managerId', ChatsController.getChat, ChatsController.setChatManagerId);
    
    router
        .get('/chats/:id/messages', ChatsController.getChat, ChatsController.getMessages)
        .post('/chats/:id/messages', ChatsController.getChat, ChatsController.createMessage);

    router
        .post('/chats/:id/close', ChatsController.getChat, ChatsController.closeChat);

    return router;
};
