'use strict';

module.exports = app => {
    let express = require('express'),
        router = express.Router();

    let CategoriesController = require('../controllers/categories-controller');
    
    router
        .get('/categories', CategoriesController.index)
        .get('/categories/:id/companies', CategoriesController.show);

    return router;
};
