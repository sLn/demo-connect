'use strict';

let SocketIO = require('socket.io');

/**
 * create socket.io server
 * 
 * @param options
 * @returns {Server}
 */
function createServer(options) {
    let server = new SocketIO(options);

    server.use((socket, next) => {
        if (true) {
            return next();
        } else {
            next(new Error('Authentication error'));
        }
    });

    server
        .on('connect', (socket) => {
            console.log('connected with id', socket.id);

            socket
                .on('room:join', socket.join)
                .on('message:typing', (chatId) => {
                    socket.broadcast.to(chatId).emit('message:typing');
                })
                .on('message:typing:stop', (chatId) => {
                    socket.broadcast.to(chatId).emit('message:typing:stop');
                })
                .on('disconnect', () => {
                    console.log('disconnect');
                });
        });

    return server;
}

module.exports = createServer;
