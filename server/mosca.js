'use strict';

const mosca = require('mosca');
const config = require('./config')();
const ascoltatore = {
    //using ascoltatore
    type: 'mongo',
    url: config.db.uri,
    pubsubCollection: 'ascoltatori',
    mongo: {}
};
const settings = {
    port: 1883,
    backend: ascoltatore
};

let models = require('./models'),
    Session = models.Session,
    Chat = models.Chat;

function setup() {
    if (config.logging) {
        console.log('Mosca server started at port', settings.port);
    }
}

function sendMessageToChatUsers(server, options, message) {
    Chat.getUserIdsForNotification(options.chat_id).then(
        userIds => {
            userIds.forEach(uId => {
                if (uId != options.user_id) {
                    server.publish({
                        topic: 'users/' + uId,
                        payload: JSON.stringify(message),
                        qos: 0,
                        retain: false
                    });
                }
            })
        },
        err => console.dir(err));
}

module.exports = () => {
    let server = new mosca.Server(settings);
    
    server.on('ready', setup);

    server.on('clientConnected', client => {
        console.log(`client connected with id ${client.id} at ${new Date}`);
    });

    server.on('clientDisconnected', client => {
        console.log('client disconnected', client.id);

        Session.disable(client.id).catch(err => console.dir(err));
    });

    server.on('published', (packet, client) => {
        let payload = (new Buffer(packet.payload)).toString();

        switch (packet.topic) {
            case 'session:new':
                var data = JSON.parse(payload);
                data.session_id = client.id;
                Session.enable(data).catch(err => console.dir(err));
                break;

            case 'chat:typing':
                var data = JSON.parse(payload);
                sendMessageToChatUsers(
                    server,
                    data,
                    {
                        type: 'chat:typing',
                        body: data
                    });
                break;

            case 'chat:typing:stop':
                var data = JSON.parse(payload);
                sendMessageToChatUsers(
                    server,
                    data,
                    {
                        type: 'chat:typing:stop',
                        body: data
                    });
                break;
        }
    });
    
    return server;
};
