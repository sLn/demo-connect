## Зависимости

* NodeJS >v4

## Разворачивание проекта

1. git clone git@bitbucket.org:sLn/demo-ssamm-connect.git
2. npm i

## Запуск проекта

`npm start`

## Запуск в окружении
`NODE_ENV=production [{STUB}_NODE_]PORT=80 [{STUB}_SOCKET]PORT=9090 npm start`

## Запуск проекта для разработки
`npm run-script dev`

## Workflow

> На bitbucket пропускаем пункты 1-3

1. Fork
1. git clone {fork}
1. git remote add upstream git@bitbucket.org:sLn/demo-ssamm-connect.git
1. Make a branch
1. Commit
1. gulp lint
1. Pull Request
1. Autotesting
1. Review
1. Merge
1. Drop branch

Разрабатываем новые фичи в ветках. После вливания ветки сносим.
Делаем коммиты с осознанным названием.
Для пулл-реквестов пишем нормальные описания.
По возможности, схлапываем N коммитов (git squash) в один в пределах ветки `git rebase -i HEAD~3`, где 3–количество коммитов для схлапывания

### Для обновления форка из апстрима, удобен bash - скрипт (~/.bash_aliases)

```bash
function master-update-origin() {
 echo "Task: Updating master branch from upstream...";
 git checkout master;
 git fetch --tags origin;
 git pull --rebase origin master;
 if [ $# -ne 0 ]
 then
    echo "Task: Returning to the branch '$1'";
    git checkout $1;
    git rebase master;
 fi
}
```

#### USAGE:
`master-update-origin` – обновить мастер, находясь в ветке master
`master-update-origin branchName` – обновить мастер, находясь в ветке branchName, и после обновления мастера вернуться в ветку и подтянуть изменения из мастера

 при установленном zsh также можно использовать
 `master-update-origin $(current_branch)` - для обновления любой текущей ветки