'use strict';

let config = require('./server/config')();

(require('./server')).init().then(
    server => {
        server.listen(config.port, () => {
            console.log('http server started at port', config.port);
            console.log('socket server started at port', config.socketPort);
        })
    },
    err => console.dir(err));
