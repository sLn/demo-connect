var gulp = require('gulp'),
    nodemon = require('gulp-nodemon'),
    browserSync = require('browser-sync').create();

gulp.task('server-start', function () {
    nodemon({
        script: 'index.js',
        watch: [
            'index.js',
            'server/*.js',
            'server/**/*.js'
        ],
        ignore: [
            './server/node_modules/*'
        ],
        ext: 'js json',
        env: {
            'NODE_ENV': 'development',
            'NODE_PORT': 4000,
            'SOCKET_PORT': 9999
        }
    }).on('restart', function () {
        setTimeout(browserSync.reload, 3000);
    });
});

gulp.task('browser-sync', function() {
    browserSync.init({
        files: [],
        notify: true,
        open: false,
        ghostMode: false,
        logLevel: 'debug',
        minify: false,
        proxy: "localhost:4000"
    });
});

gulp.task('default', ['server-start', 'browser-sync']);
